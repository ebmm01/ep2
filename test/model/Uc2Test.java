package model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Uc2Test {
    
    public Uc2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testPotAtiva() {
        System.out.println("PotAtiva");
        Uc2 instance = new Uc2();
        double expResult = 0.0;
        double result = instance.PotAtiva();
        assertEquals(expResult, result, 0.0);

    }

    @Test
    public void testPotReativa() {
        System.out.println("PotReativa");
        Uc2 instance = new Uc2();
        double expResult = 0.0;
        double result = instance.PotReativa();
        assertEquals(expResult, result, 0.0);

    }
    
    @Test
    public void testPotAparente() {
        System.out.println("PotAparente");
        Uc2 instance = new Uc2();
        double expResult = 0.0;
        double result = instance.PotAparente();
        assertEquals(expResult, result, 0.0);

    }

    @Test
    public void testFatPotencia() {
        System.out.println("FatPotencia");
        Uc2 instance = new Uc2();
        double expResult = 1.0;
        double result = instance.FatPotencia();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testStringFatPotencia() {
        System.out.println("stringFatPotencia");
        Uc2 instance = new Uc2();
        String expResult = "1";
        String result = instance.stringFatPotencia();
        assertEquals(expResult, result);
    }
 
    @Test
    public void testOndaTensao() {
        System.out.println("ondaTensao");
        Uc2 instance = new Uc2();
        ArrayList<Double> expResult = null;
        ArrayList<Double> result = instance.ondaTensao();
        assertEquals(expResult, result);
 
    }

    @Test
    public void testOndaCorrente() {
        System.out.println("ondaCorrente");
        Uc2 instance = new Uc2();
        ArrayList<Double> expResult = null;
        ArrayList<Double> result = instance.ondaCorrente();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testOndaPotInst() {
        System.out.println("ondaPotInst");
        Uc2 instance = new Uc2();
        ArrayList<Double> expResult = null;
        ArrayList<Double> result = instance.ondaPotInst();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
}
