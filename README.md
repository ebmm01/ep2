# EP2 - Instruções de Uso:

**Aluno: Elias Bernardo Marques Magalhães - 15/0009011**

Para inicializar e rodar o programa é necessário clonar o repositório:

```
mkdir *pasta*
cd *pasta*
git clone https://gitlab.com/ebmm01/ep2.git
cd ep2

```

O Projeto foi criado utilizando o Netbeans IDE 8.2, então será necessário "Abrir Projeto" no referido programa.

# Menu Principal 

Ao executar o programa o usuário irá se deparar com a seguinte tela (Menu Principal):

![Captura_de_tela_de_2017-11-15_10-54-51](/uploads/213a5b2a24578497c07323413d65995e/Captura_de_tela_de_2017-11-15_10-54-51.png)

O menu Principal possui 3 opções e uma breve explicação sobre cada uma delas:

* Simular Fluxo de Potência Fundamental
* Simular Distorção Harmônica
* Simular Fluxo de Potência Harmônico

Escolher alguma delas abrirá uma nova janela com a opção selecionada e fechará o Menu Principal.

# Fluxo de Potência Fundamental

Dentro do Fluxo de Potência Fundamental o usuário poderá observar a "forma de onda da tensão, da corrente, da potência
instantânea, o valor da potência ativa, reativa e aparente, o fator de potência e o triângulo de potências". 

Para que isso ocorra ele deverá inserir os valores de Amplitude da Tensão (entre 0 e 220) e da Corrente (entre 0 e 100) e selecionar o valor do Ângulo da Tensão e da Corrente (entre -180º e 180º).

> Qualquer valor fora dos limites citados levarão à uma mensagem de erro. É necessário inserir 3 dígitos nos valores da(s) Amplitude(s)!!!

![Captura_de_tela_de_2017-11-15_10-55-01](/uploads/e519c1f9925aa9a0b6f00d6494878f65/Captura_de_tela_de_2017-11-15_10-55-01.png)

O valor da Potência Ativa, Reativa, Aparente e do Fator de potência serão atualizados assim que um dos botões "OK" for pressionado. O mesmo vale para os Gráficos e do Triângulo de Potência.
O botão "Voltar" retorna ao Menu Principal.

# Distorção Harmônica 

Dentro da Distorção Harmônica o usuário poderá observar a "a forma de onda da componente fundamental, os harmônicos causadores das distorções e a forma de onda distorcida resultante".

Para que isso ocorra ele deverá inserir os valores da Amplitude do Componente Fundamental (entre 0 e 220) e do Ângulo do componente fundamental (entre -180º e 180º). Além disso, deverá selecionar o número de harmônicos que pretende inserir (entre 0 e 6). O programa permitirá a inserção de dados nos harmônicos de acordo com o número selecionado.

Em cada Harmônico o usuário deverá inserir o valor da Amplitude, Ângulo e Ordem do harmonico (entre 0 e 16).

> Qualquer valor fora dos limites citados levarão à uma mensagem de erro. É necessário inserir 3 dígitos nos valores da(s) Amplitude(s)!!!

![Captura_de_tela_de_2017-11-15_10-55-09](/uploads/989ad3a4b3dc107dd3a04c7a06a65929/Captura_de_tela_de_2017-11-15_10-55-09.png)

Os gráficos e a Série de Fourrier Amplitude-Fase serão atualizados automáticamente assim que um dos botões "OK" for pressionado.
O botão "Voltar" retorna ao Menu Principal.

# Fluxo de Potência Harmônico

Dentro do Fluxo de potência Harmônico o usuário poderá observar a "forma de onda tensão fundamental, do harmônico de corrente e da potência harmônica instantânea, o valor da potência líquida, o valor da potência de distorção e o valor do FP".

Para que isso ocorra ele deverá inserir os valores da Amplitude da Tensão e Corrente ( entre 0-220 e 0-100 respectivamente), do Ângulo da Tensão e da Corrente (entre -180º 3 180º) e a Ordem Harmônica da Corrente (entre 0 e 16).

> Qualquer valor fora dos limites citados levarão à uma mensagem de erro. É necessário inserir 3 dígitos nos valores da(s) Amplitude(s)!!!

![Captura_de_tela_de_2017-11-15_10-55-16](/uploads/4d625416162282ee48957816f369b304/Captura_de_tela_de_2017-11-15_10-55-16.png)

Os valores das Potências Líquida e de Distorção e do TPF serão atualizados assim que um dos botões "OK" for presionado. O mesmo vale para os gŕaficos.
O botão "voltar" retorna ao menu principal.