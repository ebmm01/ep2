
package model;

import static java.lang.Math.cos;
import java.util.ArrayList;

public class Uc4 implements Calculos{
    private double ampFund;
    private double angFund;
    private double ampHarmonico;
    private double angHarmonico;
    private double ordemHarmonico;

    
    public double getAmpFund() {
        return ampFund;
    }

    public void setAmpFund(double ampFund) {
        this.ampFund = ampFund;
    }

    public double getAngFund() {
        return angFund;
    }

    public void setAngFund(double angFund) {
        this.angFund = angFund;
    }

    public double getAmpHarmonico() {
        return ampHarmonico;
    }

    public void setAmpHarmonico(double ampHarmonico) {
        this.ampHarmonico = ampHarmonico;
    }

    public double getAngHarmonico() {
        return angHarmonico;
    }

    public void setAngHarmonico(double angHarmonico) {
        this.angHarmonico = angHarmonico;
    }

    public double getOrdemHarmonico() {
        return ordemHarmonico;
    }

    public void setOrdemHarmonico(double ordemHarmonico) {
        this.ordemHarmonico = ordemHarmonico;
    }
    
    @Override
    public ArrayList<Double> ondaTensao(){
        ArrayList<Double> fundamental = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            fundamental.add(getAmpFund()*cos((2*3.14*60*i) + getAngFund()));
        }
        return (fundamental);
    }
    
    public ArrayList<Double> ondaHarmonico(){
        ArrayList<Double> harmonico = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            harmonico.add(getAmpHarmonico()*cos((getOrdemHarmonico()*2*3.14*60*i) + getAngHarmonico()));
        }
        return (harmonico);
    }
    
    public ArrayList<Double> ondaPotInstantanea(){
        ArrayList<Double> potinstantanea = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            potinstantanea.add((getAmpFund()*cos((2*3.14*60*i) + getAngFund()))*
                    getAmpHarmonico()*cos((getOrdemHarmonico()*2*3.14*60*i) + getAngHarmonico()));
        }
        return (potinstantanea);
    }
    
    public double PotLiquida(){
        double potLiquida = 0;
        return (potLiquida);
    }
    
    public double PotDistorcao(){
        double ondaPotDistorcao = 0;
        
        ondaPotDistorcao = getAmpFund()*getAmpHarmonico();
        
        return (ondaPotDistorcao);
    }
    
    public double TPF(){
        double TPF, S = PotLiquida(),P = PotDistorcao();
        TPF= S/P;
        
        return(TPF);
    }
}
