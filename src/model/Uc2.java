package model;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;
import java.util.ArrayList;

public class Uc2 implements Calculos {
    private double AmpTensao = 0 ; //Vrms
    private double AmpCorrente = 0 ;//Irms
    private double AngTensao = 0 ;//0v
    private double AngCorrente = 0;//0i

    public double getAmpTensao() {
        return AmpTensao;
    }

    public void setAmpTensao(double AmpTensao) {
        this.AmpTensao = AmpTensao;
    }

    public double getAmpCorrente() {
        return AmpCorrente;
    }

    public void setAmpCorrente(double AmpCorrente) {
        this.AmpCorrente = AmpCorrente;
    }

    public double getAngTensao() {
        return AngTensao;
    }

    public void setAngTensao(double AngTensao) {
        this.AngTensao = AngTensao;
    }

    public double getAngCorrente() {
        return AngCorrente;
    }

    public void setAngCorrente(double AngCorrente) {
        this.AngCorrente = AngCorrente;
    }
    
    public double PotAtiva(){
        double calcPotAtiva;
        calcPotAtiva = getAmpTensao()*getAmpCorrente()*cos(toRadians(getAngTensao()-getAngCorrente()));
        calcPotAtiva = Math.round(calcPotAtiva*100.0)/100.0;
     return calcPotAtiva;
    }
    
    public double PotReativa(){
        double calcPotReativa;
        calcPotReativa = getAmpTensao()*getAmpCorrente()*sin(toRadians(getAngTensao()-getAngCorrente()));
        calcPotReativa = Math.round(calcPotReativa*100.0)/100.0;
     return calcPotReativa;
     
    }
    
    public double PotAparente(){
        double calcPotAparente;
        calcPotAparente = getAmpTensao()*getAmpCorrente();
        calcPotAparente = Math.round(calcPotAparente*100.0)/100.0;
     return calcPotAparente;
    }
    
    public double FatPotencia(){
        double calcFatPotencia;
        calcFatPotencia = cos(toRadians(getAngTensao()-getAngCorrente()));
        calcFatPotencia = Math.round(calcFatPotencia*100.0)/100.0;
     return calcFatPotencia;
    }
    public String stringFatPotencia(){
        String fatpotencia = null;
        if (getAngTensao()-getAngCorrente() < 0){
            fatpotencia = "Adiantado";
        }
        else if (getAngTensao()-getAngCorrente()>0){
            fatpotencia = "Atrasado";
        }
        else if (getAngTensao()-getAngCorrente() == 0){
            fatpotencia = "1";
        }
        return (fatpotencia);
    }
    
    @Override
    public ArrayList<Double> ondaTensao(){
    
       ArrayList<Double> tensao = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            tensao.add(getAmpTensao()*cos((2*3.14*60*i) + getAngTensao()));
        }
        return (tensao);
    }

    public ArrayList<Double> ondaCorrente(){
        ArrayList<Double> corrente = new ArrayList<>();
       corrente.clear();
        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            corrente.add(getAmpCorrente()*cos((2*3.14*60*i) + getAngCorrente()));
        }
        return (corrente);
    }
    
    public ArrayList<Double> ondaPotInst(){
      
       ArrayList<Double> potinst = new ArrayList<>();
	
        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            potinst.add((getAmpTensao()*cos((2*3.14*60*i) + getAngTensao()))*(getAmpCorrente()*cos((2*3.14*60*i) + getAngCorrente())));
        }
        return (potinst);
    }
  
}
