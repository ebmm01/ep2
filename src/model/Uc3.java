package model;

import static java.lang.Math.cos;
import java.util.ArrayList;

public class Uc3 implements Calculos{
    private double ampCompFund;
    private double angCompFund;
    private double ampHarmonico1;
    private double angHarmonico1;
    private double ampHarmonico2;
    private double angHarmonico2;
    private double ampHarmonico3;
    private double angHarmonico3;
    private double ampHarmonico4;
    private double angHarmonico4;
    private double ampHarmonico5;
    private double angHarmonico5;
    private double ampHarmonico6;
    private double angHarmonico6;
    private double ordemHarmonica1;
    private double ordemHarmonica2;
    private double ordemHarmonica3;
    private double ordemHarmonica4;
    private double ordemHarmonica5;
    private double ordemHarmonica6;

    
    public double getOrdemHarmonica1() {
        return ordemHarmonica1;
    }

    public void setOrdemHarmonica1(double ordemHarmonica1) {
        this.ordemHarmonica1 = ordemHarmonica1;
    }

    public double getOrdemHarmonica2() {
        return ordemHarmonica2;
    }

    public void setOrdemHarmonica2(double ordemHarmonica2) {
        this.ordemHarmonica2 = ordemHarmonica2;
    }

    public double getOrdemHarmonica3() {
        return ordemHarmonica3;
    }

    public void setOrdemHarmonica3(double ordemHarmonica3) {
        this.ordemHarmonica3 = ordemHarmonica3;
    }

    public double getOrdemHarmonica4() {
        return ordemHarmonica4;
    }

    public void setOrdemHarmonica4(double ordemHarmonica4) {
        this.ordemHarmonica4 = ordemHarmonica4;
    }

    public double getOrdemHarmonica5() {
        return ordemHarmonica5;
    }

    public void setOrdemHarmonica5(double ordemHarmonica5) {
        this.ordemHarmonica5 = ordemHarmonica5;
    }

    public double getOrdemHarmonica6() {
        return ordemHarmonica6;
    }

    public void setOrdemHarmonica6(double ordemHarmonica6) {
        this.ordemHarmonica6 = ordemHarmonica6;
    }
    
    public double getAmpCompFund() {
        return ampCompFund;
    }

    public void setAmpCompFund(double ampCompFund) {
        this.ampCompFund = ampCompFund;
    }

    public double getAngCompFund() {
        return angCompFund;
    }

    public void setAngCompFund(double angCompFund) {
        this.angCompFund = angCompFund;
    }

    public double getAmpHarmonico1() {
        return ampHarmonico1;
    }

    public void setAmpHarmonico1(double ampHarmonico1) {
        this.ampHarmonico1 = ampHarmonico1;
    }

    public double getAngHarmonico1() {
        return angHarmonico1;
    }

    public void setAngHarmonico1(double angHarmonico1) {
        this.angHarmonico1 = angHarmonico1;
    }

    public double getAmpHarmonico2() {
        return ampHarmonico2;
    }

    public void setAmpHarmonico2(double ampHarmonico2) {
        this.ampHarmonico2 = ampHarmonico2;
    }

    public double getAngHarmonico2() {
        return angHarmonico2;
    }

    public void setAngHarmonico2(double angHarmonico2) {
        this.angHarmonico2 = angHarmonico2;
    }

    public double getAmpHarmonico3() {
        return ampHarmonico3;
    }

    public void setAmpHarmonico3(double ampHarmonico3) {
        this.ampHarmonico3 = ampHarmonico3;
    }

    public double getAngHarmonico3() {
        return angHarmonico3;
    }

    public void setAngHarmonico3(double angHarmonico3) {
        this.angHarmonico3 = angHarmonico3;
    }

    public double getAmpHarmonico4() {
        return ampHarmonico4;
    }

    public void setAmpHarmonico4(double ampHarmonico4) {
        this.ampHarmonico4 = ampHarmonico4;
    }

    public double getAngHarmonico4() {
        return angHarmonico4;
    }

    public void setAngHarmonico4(double angHarmonico4) {
        this.angHarmonico4 = angHarmonico4;
    }

    public double getAmpHarmonico5() {
        return ampHarmonico5;
    }

    public void setAmpHarmonico5(double ampHarmonico5) {
        this.ampHarmonico5 = ampHarmonico5;
    }

    public double getAngHarmonico5() {
        return angHarmonico5;
    }

    public void setAngHarmonico5(double angHarmonico5) {
        this.angHarmonico5 = angHarmonico5;
    }

    public double getAmpHarmonico6() {
        return ampHarmonico6;
    }

    public void setAmpHarmonico6(double ampHarmonico6) {
        this.ampHarmonico6 = ampHarmonico6;
    }

    public double getAngHarmonico6() {
        return angHarmonico6;
    }

    public void setAngHarmonico6(double angHarmonico6) {
        this.angHarmonico6 = angHarmonico6;
    }
    
    @Override
    public ArrayList<Double> ondaTensao(){
        ArrayList<Double> componentefundamental = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            componentefundamental.add(getAmpCompFund()*cos((2*3.14*60*i) + getAngCompFund()));
        }
        return (componentefundamental);
    }
    
    public ArrayList<Double> ondaHarmonico1(){
        ArrayList<Double> harmonico1 = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            harmonico1.add(getAmpHarmonico1()*cos((getOrdemHarmonica1()*2*3.14*60*i) + getAngHarmonico1()));
        }
        return (harmonico1);
    }
    public ArrayList<Double> ondaHarmonico2(){
        ArrayList<Double> harmonico2 = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            harmonico2.add(getAmpHarmonico2()*cos((getOrdemHarmonica2()*2*3.14*60*i) + getAngHarmonico2()));
        }
        return (harmonico2);
    }
    
    public ArrayList<Double> ondaHarmonico3(){
        ArrayList<Double> harmonico3 = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            harmonico3.add(getAmpHarmonico3()*cos((getOrdemHarmonica3()*2*3.14*60*i) + getAngHarmonico3()));
        }
        return (harmonico3);
    }
    
    public ArrayList<Double> ondaHarmonico4(){
        ArrayList<Double> harmonico4 = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            harmonico4.add(getAmpHarmonico4()*cos((getOrdemHarmonica4()*2*3.14*60*i) + getAngHarmonico4()));
        }
        return (harmonico4);
    }
    
    public ArrayList<Double> ondaHarmonico5(){
        ArrayList<Double> harmonico5 = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            harmonico5.add(getAmpHarmonico5()*cos((getOrdemHarmonica5()*2*3.14*60*i) + getAngHarmonico5()));
        }
        return (harmonico5);
    }
    public ArrayList<Double> ondaHarmonico6(){
        ArrayList<Double> harmonico6 = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            harmonico6.add(getAmpHarmonico6()*cos((getOrdemHarmonica6()*2*3.14*60*i) + getAngHarmonico6()));
        }
        return (harmonico6);
    }
    
    public ArrayList<Double> ondaResultante(){
        ArrayList<Double> resultante = new ArrayList<>();

        for (double i = 0; i < 101; i = (float) (i + 0.1)) {
            resultante.add(getAmpCompFund()*cos((2*3.14*60*i) + getAngCompFund())+
                    getAmpHarmonico1()*cos((getOrdemHarmonica1()*2*3.14*60*i) + getAngHarmonico1()) +
                    getAmpHarmonico2()*cos((getOrdemHarmonica2()*2*3.14*60*i) + getAngHarmonico2()) +
                    getAmpHarmonico3()*cos((getOrdemHarmonica3()*2*3.14*60*i) + getAngHarmonico3()) +
                    getAmpHarmonico4()*cos((getOrdemHarmonica4()*2*3.14*60*i) + getAngHarmonico4()) +
                    getAmpHarmonico5()*cos((getOrdemHarmonica5()*2*3.14*60*i) + getAngHarmonico5()) +
                    getAmpHarmonico6()*cos((getOrdemHarmonica6()*2*3.14*60*i) + getAngHarmonico6())          
            );
        }
        return (resultante);
    }
    
    public String serie(){
        String CompFund= "",harmonico1= "",harmonico2 = "",harmonico3= "",
                harmonico4 ="",harmonico5="",harmonico6 ="", resultante = "";
        if(getAmpCompFund() != 0){
            CompFund = getAmpCompFund()+"cos(ωt + "+getAngCompFund()+")  ";
        }
        if(getAmpHarmonico1() !=0){
            harmonico1 = "+"+getAmpHarmonico1()+"cos("+getOrdemHarmonica1()+"ωt + "+getAngHarmonico1()+")  ";
        }
        if(getAmpHarmonico2() !=0){
            harmonico2 = "+"+getAmpHarmonico2()+"cos("+getOrdemHarmonica2()+"ωt + "+getAngHarmonico2()+")  ";
        }
        if(getAmpHarmonico3() !=0){
            harmonico3 = "+"+getAmpHarmonico3()+"cos("+getOrdemHarmonica3()+"ωt + "+getAngHarmonico3()+")  ";
        }
        if(getAmpHarmonico4() !=0){
            harmonico4 = "+"+getAmpHarmonico4()+"cos("+getOrdemHarmonica4()+"ωt + "+getAngHarmonico4()+")  ";
        }
        if(getAmpHarmonico5() !=0){
            harmonico5 = "+"+getAmpHarmonico5()+"cos("+getOrdemHarmonica5()+"ωt + "+getAngHarmonico5()+")  ";
        }
        if(getAmpHarmonico6() !=0){
            harmonico6 = "+"+getAmpHarmonico6()+"cos("+getOrdemHarmonica6()+"ωt + "+getAngHarmonico6()+")  ";
        }
        
        resultante = "f(t)= "+CompFund+harmonico1+harmonico2+harmonico3+harmonico4+harmonico5+harmonico6;
         
        return (resultante);
    }
}
