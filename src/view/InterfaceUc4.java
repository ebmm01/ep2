package view;

import model.*;
import controllers.*;
import java.awt.Font;
import java.awt.event.*;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.*;
import java.util.ArrayList;
import javax.swing.text.MaskFormatter;
public class InterfaceUc4 {

   private JFrame mainFrame;
   private JPanel panelFund;
   private JPanel panelHarmonico;
   private JPanel panelSaidas;
   private AcoesUc4 Listeners;

   public InterfaceUc4() throws IOException{
    try { 
    UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel"); 
    } catch (Exception ex) { 
    ex.printStackTrace(); 
    }
    preparaGUI();
    }
       
   private void preparaGUI(){
    mainFrame = new JFrame("Fluxo de Potência Harmônico");
    mainFrame.setSize(850,630);
    mainFrame.setLayout(null);
    mainFrame.setLocationRelativeTo(null);

    mainFrame.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent windowEvent){
	    System.exit(0);
         }        
      });
    
    panelFund = new JPanel();
    panelFund.setLayout(null);
    panelFund.setBounds(20,30,810,180);
    panelFund.setBorder(javax.swing.BorderFactory.createTitledBorder("Tensao"));

    panelHarmonico = new JPanel();
    panelHarmonico.setLayout(null);
    panelHarmonico.setBorder(javax.swing.BorderFactory.createTitledBorder("Corrente"));
    panelHarmonico.setBounds(20,220,810,180);

    panelSaidas = new JPanel();
    panelSaidas.setLayout(null);
    panelSaidas.setBorder(javax.swing.BorderFactory.createTitledBorder("Potência Harmônica Instantânea"));
    panelSaidas.setBounds(20,410,810,180);
    
    preparaJPanels();
    mainFrame.add(panelFund);
    mainFrame.add(panelHarmonico);
    mainFrame.add(panelSaidas);
    mainFrame.setVisible(true);  
   }
   public void preparaJPanels(){
       Listeners = new AcoesUc4();
       //Fundamental
        JLabel labelAmpFund = new JLabel("Amplitude:");
        JLabel labelAngFund = new JLabel("Ângulo da Fase:");
        JButton okFundButton = new JButton("OK");
        
        labelAmpFund.setBounds(15,30,200,20);
        labelAngFund.setBounds(15,60,200,20);
        okFundButton.setBounds(100,120,54,30);
       
        MaskFormatter entraAmpFund = null; 
             
        try{
            entraAmpFund = new MaskFormatter("###");
   
        }
        catch(ParseException excp) {
            System.err.println("Erro na formatação: " + excp.getMessage());
            System.exit(-1);
        }
     
        JFormattedTextField jFormattedAmpFund = new JFormattedTextField(entraAmpFund);
        jFormattedAmpFund.setValue("000");
        jFormattedAmpFund.setToolTipText("É necessário colocar 3 números. Se o número for 1, insira 001.");
        
        JComboBox jcomboAngFund = new JComboBox();
        for (int i = -180; i<181; i++){
            jcomboAngFund.addItem(i);
        }
        jcomboAngFund.setSelectedItem(0);
        
        okFundButton.setActionCommand("okFund");
        okFundButton.addActionListener(Listeners);
        okFundButton.setToolTipText("Clique para inserir os dados.");
     
        jFormattedAmpFund.setBounds(163,30,35,22);
        jcomboAngFund.setBounds(140,60,58,20);
        
        Uc4 graficoondaFund = new Uc4();
        ArrayList<Double> Fund = graficoondaFund.ondaTensao();
        Grafico graficoFund = new Grafico(Fund,false);
        graficoFund.setBounds(250,15,540,155);
     
        panelFund.add(labelAmpFund);
        panelFund.add(jFormattedAmpFund);
        panelFund.add(labelAngFund);
        panelFund.add(jcomboAngFund);
        panelFund.add(okFundButton);  
        panelFund.add(graficoFund);  
        //Fim do Painel Fundamental
        
        //Corrente
        JLabel labelOrdemHarmonico = new JLabel("Ordem Harmônica:");
        JLabel labelAmpHarmonico = new JLabel("Amplitude:");
        JLabel labelAngHarmonico = new JLabel("Ângulo:");
       
        JButton okHarmonicos = new JButton("OK");
        
        MaskFormatter entraAmpHarmonico = null; 
       
        try{
            entraAmpHarmonico = new MaskFormatter("###");
        }
        catch(ParseException excp) {
            System.err.println("Erro na formatação: " + excp.getMessage());
            System.exit(-1);
        }
     
        JFormattedTextField jFormattedAmpHarmonico = new JFormattedTextField(entraAmpHarmonico);
        jFormattedAmpHarmonico.setValue("000");
        jFormattedAmpHarmonico.setToolTipText("É necessário colocar 3 números. Se o número for 1, insira 001.");
        
        JComboBox jcomboAngHarmonico = new JComboBox();
        JComboBox jcomboOrdemHarmonico = new JComboBox();
        
        
        Uc4 graficoHarmonico = new Uc4();
        
        ArrayList<Double> harmonico = graficoHarmonico.ondaHarmonico();
            
        Grafico Harmonico = new Grafico(harmonico,false);

        
        
        
        for (int i = -180; i<181; i++){
            jcomboAngHarmonico.addItem(i);
           
        }
        for (int i = 0; i<17; i++){
            jcomboOrdemHarmonico.addItem(i);
        }
        jcomboAngHarmonico.setSelectedItem(0);
  
        
        
        okHarmonicos.setActionCommand("okHarmonico");
        okHarmonicos.setToolTipText("Clique para inserir os dados.");
        okHarmonicos.addActionListener(Listeners);
        okHarmonicos.setBounds(100,130,54,30);
                
        labelAmpHarmonico.setBounds(15,30,200,20);
        labelAngHarmonico.setBounds(15,60,200,20);        
        labelOrdemHarmonico.setBounds(15,90,200,20);
        jcomboAngHarmonico.setBounds(150,60,58,20);
        jFormattedAmpHarmonico.setBounds(173,30,35,22);
        jcomboOrdemHarmonico.setBounds(150,90,58,20);
        Harmonico.setBounds(250,15,540,155);
        
       
                
        panelHarmonico.add(okHarmonicos);
        panelHarmonico.add(labelAmpHarmonico);
        panelHarmonico.add(labelAngHarmonico);
        panelHarmonico.add(labelOrdemHarmonico);
        panelHarmonico.add(jcomboAngHarmonico);
        panelHarmonico.add(jFormattedAmpHarmonico);
        panelHarmonico.add(jcomboOrdemHarmonico);
        panelHarmonico.add(Harmonico);
        //Fim do Painel Corrente
        
        //Saida
        JLabel labelPotLiquid = new JLabel("Potência Líquida:");
        JLabel labelPotDistor = new JLabel("Potência de Distorção:");
        JLabel labelTPF = new JLabel("TPF:");
        
        JTextField textPotLiquid = new JTextField();
        JTextField textPotDistor = new JTextField();
        JTextField textTPF = new JTextField();
        
        textPotLiquid.setEditable(false);
        textPotDistor.setEditable(false);
        textTPF.setEditable(false);
        
        labelPotLiquid.setBounds(35,30,200,20);
        labelPotDistor.setBounds(35,75,200,20);
        labelTPF.setBounds(40,120,200,20);
        textPotLiquid.setBounds(35,50,170,25);
        textPotDistor.setBounds(35,95,170,25);
        textTPF.setBounds(35,140,170,25);
        
        Uc4 graficoPotinst = new Uc4();
        ArrayList<Double> resultante = graficoPotinst.ondaPotInstantanea();
        Grafico graficoresulaPotInst = new Grafico(resultante,true);
        graficoresulaPotInst.setBounds(250,15,540,155);
        
       
        panelSaidas.add(labelPotLiquid);
        panelSaidas.add(labelPotDistor);
        panelSaidas.add(labelTPF);
        panelSaidas.add(graficoresulaPotInst);
        panelSaidas.add(textPotLiquid);
        panelSaidas.add(textPotDistor);
        panelSaidas.add(textTPF);
        //Fim da Saida
        
         //Voltar
        JLabel tituloUc4 = new JLabel("Simular Fluxo de Potência Harmonico");
        JButton voltar = new JButton("Voltar");
        voltar.setBounds(775,1,54,30);
        voltar.setActionCommand("voltarUc4");
        tituloUc4.setBounds(50,1,1000,30);
        tituloUc4.setFont(new Font("Dialog", Font.BOLD, 14));
        voltar.addActionListener(Listeners);
        mainFrame.add(voltar);
        mainFrame.add(tituloUc4);
        //Fim do Voltar
        
        Listeners.Saidas(mainFrame, textPotLiquid, textPotDistor, textTPF, graficoresulaPotInst);
        Listeners.entradaFundamental(jFormattedAmpFund, jcomboAngFund, graficoFund);
        Listeners.entradaHarmonico(jFormattedAmpHarmonico, jcomboAngHarmonico, jcomboOrdemHarmonico, Harmonico);
   }

}




