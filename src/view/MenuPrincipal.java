package view;

import controllers.AcoesMenuPrincipal;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import javax.swing.*;

public class MenuPrincipal {
   private JFrame mainFrame;
   private JPanel panelUc2;
   private JPanel panelUc3;
   private JPanel panelUc4;
   
   public MenuPrincipal() throws IOException{
    try { 
    UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel"); 
    } catch (Exception ex) { 
    ex.printStackTrace(); 
    }
    preparaGUI();
    }
   
   private void preparaGUI(){
    mainFrame = new JFrame("Menu Principal");
    mainFrame.setSize(660,300);
    mainFrame.setLayout(new GridLayout(1,3));
    mainFrame.setLocationRelativeTo(null);

    mainFrame.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent windowEvent){
	    System.exit(0);
         }        
      });
    
    panelUc2 = new JPanel();
    panelUc2.setLayout(null);
    panelUc2.setBorder(javax.swing.BorderFactory.createTitledBorder("Uc2"));
    
    panelUc3 = new JPanel();
    panelUc3.setLayout(null);
    panelUc3.setBorder(javax.swing.BorderFactory.createTitledBorder("Uc3"));
    
    panelUc4 = new JPanel();
    panelUc4.setLayout(null);
    panelUc4.setBorder(javax.swing.BorderFactory.createTitledBorder("Uc4"));
    
    preparaJpanel();
    mainFrame.add(panelUc2);
    mainFrame.add(panelUc3);
    mainFrame.add(panelUc4);
    mainFrame.setVisible(true);
    
    
   }
   
   private void preparaJpanel(){
        //Uc2
        JTextArea uc2 = new JTextArea("Simular Fluxo de Potência     Fundamental");
        JTextArea uc2info = new JTextArea("     Mostrar a forma de onda da tensão, da corrente, da potência instantânea, o valor da potência ativa, reativa e aparente, o fator de potência e o triângulo de potências.");
        JButton buttonUc2 = new JButton("OK");
        uc2.setEditable(false);
        uc2.setLineWrap(true);
        uc2.setWrapStyleWord(true);
        uc2.setBorder(BorderFactory.createEmptyBorder());
        uc2.setOpaque(false);
        uc2info.setEditable(false);
        uc2info.setLineWrap(true);
        uc2info.setWrapStyleWord(true);
        uc2info.setBorder(BorderFactory.createEmptyBorder());
        uc2info.setOpaque(false);
        uc2.setFont(new Font("Dialog", Font.BOLD, 12));
        
        buttonUc2.setActionCommand("okUc2");
        buttonUc2.addActionListener(new AcoesMenuPrincipal(mainFrame));
        uc2.setBounds(10,20,210,50);
        uc2info.setBounds(10,70,210,160);
        buttonUc2.setBounds(85,230,54,30);
        panelUc2.add(uc2);
        panelUc2.add(uc2info);
        panelUc2.add(buttonUc2);
        //Fim do Uc2
        
        //Uc3
        JTextArea uc3 = new JTextArea("Simular Distorção Harmônica");
        JTextArea uc3info = new JTextArea("     Aresenta a forma de onda da componente fundamental, os harmônicos causadores das distorções e a forma de onda distorcida resultante.");
        JButton buttonUc3 = new JButton("OK");
        uc3.setEditable(false);
        uc3.setLineWrap(true);
        uc3.setWrapStyleWord(true);
        uc3.setBorder(BorderFactory.createEmptyBorder());
        uc3.setOpaque(false);
        uc3info.setEditable(false);
        uc3info.setLineWrap(true);
        uc3info.setWrapStyleWord(true);
        uc3info.setBorder(BorderFactory.createEmptyBorder());
        uc3info.setOpaque(false);
        uc3.setFont(new Font("Dialog", Font.BOLD, 12));
       
        buttonUc3.setActionCommand("okUc3");
        buttonUc3.addActionListener(new AcoesMenuPrincipal(mainFrame));
        uc3.setBounds(10,20,210,50);
        uc3info.setBounds(10,60,210,160);
        buttonUc3.setBounds(85,230,54,30);
        panelUc3.add(uc3);
        panelUc3.add(uc3info);
        panelUc3.add(buttonUc3);
        //Fim do Uc3
        
        //Uc4
        JTextArea uc4 = new JTextArea("Simular Fluxo de Potência Harmônico");
        JTextArea uc4info = new JTextArea("     Mostra a forma de onda tensão fundamental, do harmônico de corrente e da potência harmônica instantânea, o valor da potência líquida, o valor da potência de distorção e o valor do TFP");
        JButton buttonUc4 = new JButton("OK");
        uc4.setEditable(false);
        uc4.setLineWrap(true);
        uc4.setWrapStyleWord(true);
        uc4.setBorder(BorderFactory.createEmptyBorder());
        uc4.setOpaque(false);
        uc4info.setEditable(false);
        uc4info.setLineWrap(true);
        uc4info.setWrapStyleWord(true);
        uc4info.setBorder(BorderFactory.createEmptyBorder());
        uc4info.setOpaque(false);
        uc4.setFont(new Font("Dialog", Font.BOLD, 12));
       
        buttonUc4.setActionCommand("okUc4");
        buttonUc4.addActionListener(new AcoesMenuPrincipal(mainFrame));
        uc4.setBounds(10,20,210,50);
        uc4info.setBounds(10,60,210,160);
        buttonUc4.setBounds(85,230,54,30);
        panelUc4.add(uc4);
        panelUc4.add(uc4info);
        panelUc4.add(buttonUc4);
        //Fim do Uc4
    }
    
}
