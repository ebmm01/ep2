package view;

import model.*;
import controllers.*;
import java.awt.Font;
import java.awt.event.*;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.*;
import java.util.ArrayList;
import javax.swing.text.MaskFormatter;
public class InterfaceUc3 {

   private JFrame mainFrame;
   private JPanel panelComponenteFundamental;
   private JPanel panelHarmonicas;
   private JPanel panelSaidas;
   private AcoesUc3 listeners;

   public InterfaceUc3() throws IOException{
    try { 
    UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel"); 
    } catch (Exception ex) { 
    ex.printStackTrace(); 
    }
    preparaGUI();
    }

   private void preparaGUI(){
    mainFrame = new JFrame("Distorção Harmônica");
    mainFrame.setSize(1300,690);
    mainFrame.setLayout(null);
    mainFrame.setLocationRelativeTo(null);

    mainFrame.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent windowEvent){
	    System.exit(0);
         }        
      });
    
    panelComponenteFundamental = new JPanel();
    panelComponenteFundamental.setLayout(null);
    panelComponenteFundamental.setBounds(10,30,430,230);
    panelComponenteFundamental.setBorder(javax.swing.BorderFactory.createTitledBorder("Componente Fundamental"));

    panelHarmonicas = new JPanel();
    panelHarmonicas.setLayout(null);
    panelHarmonicas.setBorder(javax.swing.BorderFactory.createTitledBorder("Harmônicas"));
    panelHarmonicas.setBounds(450,30,850,625);

    panelSaidas = new JPanel();
    panelSaidas.setLayout(null);
    panelSaidas.setBorder(javax.swing.BorderFactory.createTitledBorder("Saídas"));
    panelSaidas.setBounds(10,270,430,385);
    
    preparaJPanels();
    mainFrame.add(panelComponenteFundamental);
    mainFrame.add(panelHarmonicas);
    mainFrame.add(panelSaidas);
    mainFrame.setVisible(true);  
   }
   public void preparaJPanels(){
       listeners = new AcoesUc3();
       //ComponenteFundamental
        JLabel labelAmpCompFund = new JLabel("Amplitude:");
        JLabel labelAngCompFund = new JLabel("Ângulo da Fase:");
        JButton okCompFundButton = new JButton("OK");
        
        labelAmpCompFund.setBounds(15,30,200,20);
        labelAngCompFund.setBounds(140,30,200,20);
        okCompFundButton.setBounds(350,25,54,30);
        okCompFundButton.addActionListener(listeners);
        MaskFormatter entraAmpCompFund = null; 
             
        try{
            entraAmpCompFund = new MaskFormatter("###");
   
        }
        catch(ParseException excp) {
            System.err.println("Erro na formatação: " + excp.getMessage());
            System.exit(-1);
        }
     
        JFormattedTextField jFormattedAmpCompFund = new JFormattedTextField(entraAmpCompFund);
        jFormattedAmpCompFund.setValue("000");
        jFormattedAmpCompFund.setToolTipText("É necessário colocar 3 números. Se o número for 1, insira 001.");
        
        JComboBox jcomboCompFund = new JComboBox();
        for (int i = -180; i<181; i++){
            jcomboCompFund.addItem(i);
        }
        jcomboCompFund.setSelectedItem(0);
        
        okCompFundButton.setActionCommand("okCompFund");
        okCompFundButton.setToolTipText("Clique para inserir os dados.");
     
        jFormattedAmpCompFund.setBounds(90,30,35,22);
        jcomboCompFund.setBounds(250,30,58,20);
        
        Uc3 graficoondaCompFund = new Uc3();
        ArrayList<Double> CompFund = graficoondaCompFund.ondaTensao();
        Grafico graficoCompFund = new Grafico(CompFund,false);
        graficoCompFund.setBounds(15,65,390,150);
     
        panelComponenteFundamental.add(labelAmpCompFund);
        panelComponenteFundamental.add(jFormattedAmpCompFund);
        panelComponenteFundamental.add(labelAngCompFund);
        panelComponenteFundamental.add(jcomboCompFund);
        panelComponenteFundamental.add(okCompFundButton);  
        panelComponenteFundamental.add(okCompFundButton);  
        panelComponenteFundamental.add(graficoCompFund);
        //Fim do ComponenteFundamental
        
        //Saida
        JLabel labelInformacoes = new JLabel("Série de Fourrier Amplitude-Fase:");
        JTextArea resultante = new JTextArea();       
        
        resultante.setBounds(15,50,390,70);
        resultante.setEditable(false);
        resultante.setLineWrap(true);
        resultante.setBorder(BorderFactory.createEmptyBorder());
        labelInformacoes.setBounds(100,25,250,20);

        Uc3 graficoondaResultante = new Uc3();
        ArrayList<Double> ondaresultante = graficoondaCompFund.ondaTensao();
        Grafico graficoResultante = new Grafico(ondaresultante,true);
        graficoResultante.setBounds(15,200,390,170);
        
        panelSaidas.add(resultante);
        panelSaidas.add(labelInformacoes);
        panelSaidas.add(graficoResultante);

        //Fim da Saida
        
        //Painel Harmônicos
        JLabel labelNHarmonicos = new JLabel("Número de Harmônicos:");
        
        JLabel labelOrdemHarmonico1 = new JLabel("Ordem:");
        JLabel labelAmpHarmonico1 = new JLabel("Amplitude:");
        JLabel labelAngHarmonico1 = new JLabel("Ângulo:");
        JLabel labelOrdemHarmonico2 = new JLabel("Ordem:");
        JLabel labelAmpHarmonico2 = new JLabel("Amplitude:");
        JLabel labelAngHarmonico2 = new JLabel("Ângulo:");
        JLabel labelOrdemHarmonico3 = new JLabel("Ordem:");
        JLabel labelAmpHarmonico3 = new JLabel("Amplitude:");
        JLabel labelAngHarmonico3 = new JLabel("Ângulo:");
        JLabel labelOrdemHarmonico4 = new JLabel("Ordem:");
        JLabel labelAmpHarmonico4 = new JLabel("Amplitude:");
        JLabel labelAngHarmonico4 = new JLabel("Ângulo:");
        JLabel labelOrdemHarmonico5 = new JLabel("Ordem:");
        JLabel labelAmpHarmonico5 = new JLabel("Amplitude:");
        JLabel labelAngHarmonico5 = new JLabel("Ângulo:");
        JLabel labelOrdemHarmonico6 = new JLabel("Ordem:");
        JLabel labelAmpHarmonico6 = new JLabel("Amplitude:");
        JLabel labelAngHarmonico6 = new JLabel("Ângulo:");
        JButton okHarmonicos = new JButton("OK");
        
        MaskFormatter entraAmpHarmonico1 = null; 
        MaskFormatter entraAmpHarmonico2 = null; 
        MaskFormatter entraAmpHarmonico3 = null; 
        MaskFormatter entraAmpHarmonico4 = null; 
        MaskFormatter entraAmpHarmonico5 = null; 
        MaskFormatter entraAmpHarmonico6 = null; 
        try{
            entraAmpHarmonico1 = new MaskFormatter("###");
            entraAmpHarmonico2 = new MaskFormatter("###");
            entraAmpHarmonico3 = new MaskFormatter("###");
            entraAmpHarmonico4 = new MaskFormatter("###");
            entraAmpHarmonico5 = new MaskFormatter("###");
            entraAmpHarmonico6 = new MaskFormatter("###");
   
        }
        catch(ParseException excp) {
            System.err.println("Erro na formatação: " + excp.getMessage());
            System.exit(-1);
        }
     
        JFormattedTextField jFormattedAmpHarmonico1 = new JFormattedTextField(entraAmpHarmonico1);
        JFormattedTextField jFormattedAmpHarmonico2 = new JFormattedTextField(entraAmpHarmonico2);
        JFormattedTextField jFormattedAmpHarmonico3 = new JFormattedTextField(entraAmpHarmonico3);
        JFormattedTextField jFormattedAmpHarmonico4 = new JFormattedTextField(entraAmpHarmonico4);
        JFormattedTextField jFormattedAmpHarmonico5 = new JFormattedTextField(entraAmpHarmonico5);
        JFormattedTextField jFormattedAmpHarmonico6 = new JFormattedTextField(entraAmpHarmonico6);
        jFormattedAmpHarmonico1.setValue("000");
        jFormattedAmpHarmonico2.setValue("000");
        jFormattedAmpHarmonico3.setValue("000");
        jFormattedAmpHarmonico4.setValue("000");
        jFormattedAmpHarmonico5.setValue("000");
        jFormattedAmpHarmonico6.setValue("000");
        jFormattedAmpHarmonico1.setToolTipText("É necessário colocar 3 números. Se o número for 1, insira 001.");
        
        JComboBox jcomboNHarmonicos = new JComboBox();
        JComboBox jcomboAngHarmonico1 = new JComboBox();
        JComboBox jcomboAngHarmonico2 = new JComboBox();
        JComboBox jcomboAngHarmonico3 = new JComboBox();
        JComboBox jcomboAngHarmonico4 = new JComboBox();
        JComboBox jcomboAngHarmonico5 = new JComboBox();
        JComboBox jcomboAngHarmonico6 = new JComboBox();
        
        JComboBox jcomboOrdemHarmonico1 = new JComboBox();
        JComboBox jcomboOrdemHarmonico2 = new JComboBox();
        JComboBox jcomboOrdemHarmonico3 = new JComboBox();
        JComboBox jcomboOrdemHarmonico4 = new JComboBox();
        JComboBox jcomboOrdemHarmonico5 = new JComboBox();
        JComboBox jcomboOrdemHarmonico6 = new JComboBox();
        
        Uc3 graficoondaHarmonico1 = new Uc3();
        Uc3 graficoondaHarmonico2 = new Uc3();
        Uc3 graficoondaHarmonico3 = new Uc3();
        Uc3 graficoondaHarmonico4 = new Uc3();
        Uc3 graficoondaHarmonico5 = new Uc3();
        Uc3 graficoondaHarmonico6 = new Uc3();
        ArrayList<Double> harmonico1 = graficoondaHarmonico1.ondaHarmonico1();
        ArrayList<Double> harmonico2 = graficoondaHarmonico2.ondaHarmonico2();
        ArrayList<Double> harmonico3 = graficoondaHarmonico3.ondaHarmonico3();
        ArrayList<Double> harmonico4 = graficoondaHarmonico4.ondaHarmonico4();
        ArrayList<Double> harmonico5 = graficoondaHarmonico5.ondaHarmonico5();
        ArrayList<Double> harmonico6 = graficoondaHarmonico6.ondaHarmonico6();
        
        Grafico graficoHarmonico1 = new Grafico(harmonico1,false);
        Grafico graficoHarmonico2 = new Grafico(harmonico2,false);
        Grafico graficoHarmonico3 = new Grafico(harmonico3,false);
        Grafico graficoHarmonico4 = new Grafico(harmonico4,false);
        Grafico graficoHarmonico5 = new Grafico(harmonico5,false);
        Grafico graficoHarmonico6 = new Grafico(harmonico6,false);
        
        
        
        for (int i = 0; i<7;i++){
            jcomboNHarmonicos.addItem(i);
        }
        for (int i = -180; i<181; i++){
            jcomboAngHarmonico1.addItem(i);
            jcomboAngHarmonico2.addItem(i);
            jcomboAngHarmonico3.addItem(i);
            jcomboAngHarmonico4.addItem(i);
            jcomboAngHarmonico5.addItem(i);
            jcomboAngHarmonico6.addItem(i);
        }
        for (int i = 0; i<17; i++){
            jcomboOrdemHarmonico1.addItem(i);
            jcomboOrdemHarmonico2.addItem(i);
            jcomboOrdemHarmonico3.addItem(i);
            jcomboOrdemHarmonico4.addItem(i);
            jcomboOrdemHarmonico5.addItem(i);
            jcomboOrdemHarmonico6.addItem(i);
        }
        jcomboAngHarmonico1.setSelectedItem(0);
        jcomboAngHarmonico2.setSelectedItem(0);
        jcomboAngHarmonico3.setSelectedItem(0);
        jcomboAngHarmonico4.setSelectedItem(0);
        jcomboAngHarmonico5.setSelectedItem(0);
        jcomboAngHarmonico6.setSelectedItem(0);
        
        
        okHarmonicos.setActionCommand("okHarmonicos");
        
        okHarmonicos.setToolTipText("Clique para inserir os dados.");
        
        okHarmonicos.setBounds(790,15,54,30);
        labelNHarmonicos.setBounds(15,15,200,20);
        jcomboNHarmonicos.setBounds(195,15,50,20);
        
        labelAmpHarmonico1.setBounds(15,68,150,20);
        labelAngHarmonico1.setBounds(150,68,150,20);        
        labelOrdemHarmonico1.setBounds(285,68,150,20);
        jcomboAngHarmonico1.setBounds(210,70,54,20);
        jFormattedAmpHarmonico1.setBounds(95,70,35,22);
        jcomboOrdemHarmonico1.setBounds(345,70,50,20);
        graficoHarmonico1.setBounds(15,95,380,140);
        
        labelAmpHarmonico2.setBounds(15,253,150,20);
        labelAngHarmonico2.setBounds(150,253,150,20);        
        labelOrdemHarmonico2.setBounds(285,253,150,20);
        jcomboAngHarmonico2.setBounds(210,255,54,20);
        jFormattedAmpHarmonico2.setBounds(95,255,35,22);
        jcomboOrdemHarmonico2.setBounds(345,255,50,20);
        graficoHarmonico2.setBounds(15,280,380,140);
        
        labelAmpHarmonico3.setBounds(15,438,150,20);
        labelAngHarmonico3.setBounds(150,438,150,20);        
        labelOrdemHarmonico3.setBounds(285,438,150,20);
        jcomboAngHarmonico3.setBounds(210,440,54,20);
        jFormattedAmpHarmonico3.setBounds(95,440,35,22);
        jcomboOrdemHarmonico3.setBounds(345,440,50,20);
        graficoHarmonico3.setBounds(15,465,380,140);
        
        labelAmpHarmonico4.setBounds(455,68,150,20);
        labelAngHarmonico4.setBounds(590,68,150,20);        
        labelOrdemHarmonico4.setBounds(725,68,150,20);
        jcomboAngHarmonico4.setBounds(650,70,54,20);
        jFormattedAmpHarmonico4.setBounds(535,68,35,22);
        jcomboOrdemHarmonico4.setBounds(785,70,50,20);
        graficoHarmonico4.setBounds(455,95,380,140);
        
        labelAmpHarmonico5.setBounds(455,253,150,20);
        labelAngHarmonico5.setBounds(590,253,150,20);        
        labelOrdemHarmonico5.setBounds(725,253,150,20);
        jcomboAngHarmonico5.setBounds(650,255,54,20);
        jFormattedAmpHarmonico5.setBounds(535,255,35,22);
        jcomboOrdemHarmonico5.setBounds(785,255,50,20);
        graficoHarmonico5.setBounds(455,280,380,140);
        
        labelAmpHarmonico6.setBounds(455,440,150,20);
        labelAngHarmonico6.setBounds(590,440,150,20);        
        labelOrdemHarmonico6.setBounds(725,440,150,20);
        jcomboAngHarmonico6.setBounds(650,440,54,20);
        jFormattedAmpHarmonico6.setBounds(535,440,35,22);
        jcomboOrdemHarmonico6.setBounds(785,440,50,20);
        graficoHarmonico6.setBounds(455,465,380,140);
        
        jcomboAngHarmonico1.setEnabled(false);
        jFormattedAmpHarmonico1.setEnabled(false);
        jcomboOrdemHarmonico1.setEnabled(false);
        
        jcomboAngHarmonico2.setEnabled(false);
        jFormattedAmpHarmonico2.setEnabled(false);
        jcomboOrdemHarmonico2.setEnabled(false);
        
        jcomboAngHarmonico3.setEnabled(false);
        jFormattedAmpHarmonico3.setEnabled(false);
        jcomboOrdemHarmonico3.setEnabled(false);
        
        jcomboAngHarmonico4.setEnabled(false);
        jFormattedAmpHarmonico4.setEnabled(false);
        jcomboOrdemHarmonico4.setEnabled(false);
        
        jcomboAngHarmonico5.setEnabled(false);
        jFormattedAmpHarmonico5.setEnabled(false);
        jcomboOrdemHarmonico5.setEnabled(false);
        
        jcomboAngHarmonico6.setEnabled(false);
        jFormattedAmpHarmonico6.setEnabled(false);
        jcomboOrdemHarmonico6.setEnabled(false);
        
        okHarmonicos.addActionListener(listeners);
        jcomboNHarmonicos.addActionListener(listeners);
        panelHarmonicas.add(okHarmonicos);
        panelHarmonicas.add(labelNHarmonicos);
        panelHarmonicas.add(jcomboNHarmonicos);
        panelHarmonicas.add(labelAmpHarmonico1);
        panelHarmonicas.add(labelAngHarmonico1);
        panelHarmonicas.add(labelOrdemHarmonico1);
        panelHarmonicas.add(jcomboAngHarmonico1);
        panelHarmonicas.add(jFormattedAmpHarmonico1);
        panelHarmonicas.add(jcomboOrdemHarmonico1);
        panelHarmonicas.add(graficoHarmonico1);
        
        panelHarmonicas.add(labelAmpHarmonico2);
        panelHarmonicas.add(labelAngHarmonico2);
        panelHarmonicas.add(labelOrdemHarmonico2);
        panelHarmonicas.add(jcomboAngHarmonico2);
        panelHarmonicas.add(jFormattedAmpHarmonico2);
        panelHarmonicas.add(jcomboOrdemHarmonico2);
        panelHarmonicas.add(graficoHarmonico2);
        
        panelHarmonicas.add(labelAmpHarmonico3);
        panelHarmonicas.add(labelAngHarmonico3);
        panelHarmonicas.add(labelOrdemHarmonico3);
        panelHarmonicas.add(jcomboAngHarmonico3);
        panelHarmonicas.add(jFormattedAmpHarmonico3);
        panelHarmonicas.add(jcomboOrdemHarmonico3);
        panelHarmonicas.add(graficoHarmonico3);
        
        panelHarmonicas.add(labelAmpHarmonico4);
        panelHarmonicas.add(labelAngHarmonico4);
        panelHarmonicas.add(labelOrdemHarmonico4);
        panelHarmonicas.add(jcomboAngHarmonico4);
        panelHarmonicas.add(jFormattedAmpHarmonico4);
        panelHarmonicas.add(jcomboOrdemHarmonico4);
        panelHarmonicas.add(graficoHarmonico4);
        
        panelHarmonicas.add(labelAmpHarmonico5);
        panelHarmonicas.add(labelAngHarmonico5);
        panelHarmonicas.add(labelOrdemHarmonico5);
        panelHarmonicas.add(jcomboAngHarmonico5);
        panelHarmonicas.add(jFormattedAmpHarmonico5);
        panelHarmonicas.add(jcomboOrdemHarmonico5);
        panelHarmonicas.add(graficoHarmonico5);
        
        panelHarmonicas.add(labelAmpHarmonico6);
        panelHarmonicas.add(labelAngHarmonico6);
        panelHarmonicas.add(labelOrdemHarmonico6);
        panelHarmonicas.add(jcomboAngHarmonico6);
        panelHarmonicas.add(jFormattedAmpHarmonico6);
        panelHarmonicas.add(jcomboOrdemHarmonico6);
        panelHarmonicas.add(graficoHarmonico6);
        //Fim do Painel Harmônicos
        
         //Voltar
        JLabel tituloUc3 = new JLabel("Simular Distorção Harmonica");
        JButton voltar = new JButton("Voltar");
        voltar.setBounds(1240,2,54,30);
        voltar.setActionCommand("voltarUc3");
        tituloUc3.setBounds(50,1,1000,30);
        tituloUc3.setFont(new Font("Dialog", Font.BOLD, 14));
        voltar.addActionListener(listeners);
        mainFrame.add(voltar);
        mainFrame.add(tituloUc3);
        //Fim do Voltar
        
        listeners.saidas(mainFrame, resultante);
        listeners.entradasCompFund(jFormattedAmpCompFund, jcomboCompFund, graficoCompFund);
        listeners.entradasHarmonicos( jFormattedAmpHarmonico1,jFormattedAmpHarmonico2,jFormattedAmpHarmonico3,jFormattedAmpHarmonico4,jFormattedAmpHarmonico5,jFormattedAmpHarmonico6,
                 jcomboNHarmonicos,
                 jcomboAngHarmonico1,jcomboAngHarmonico2,jcomboAngHarmonico3,jcomboAngHarmonico4,jcomboAngHarmonico5,jcomboAngHarmonico6,jcomboOrdemHarmonico1,
                 jcomboOrdemHarmonico2, jcomboOrdemHarmonico3,jcomboOrdemHarmonico4,jcomboOrdemHarmonico5,jcomboOrdemHarmonico6);
        listeners.graficosHarmonicos(graficoHarmonico1, graficoHarmonico2, graficoHarmonico3, graficoHarmonico4, graficoHarmonico5, graficoHarmonico6);
        listeners.graficoResultante(graficoResultante);
   }

}




