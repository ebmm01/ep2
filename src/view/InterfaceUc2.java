package view;

import model.Uc2;
import controllers.AcoesUc2;
import controllers.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.*;
import java.util.ArrayList;
import javax.swing.text.MaskFormatter;
public class InterfaceUc2 {

   private JFrame mainFrame;
   private JPanel entradasTensao;
   private JPanel entradasCorrente;
   private JPanel saidas;
   private JPanel panelGrafico;
   private JPanel trianguloPotencias;
   private AcoesUc2 Listeners;

   public InterfaceUc2()throws IOException{
    try { 
    UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel"); 
    } catch (Exception ex) { 
    ex.printStackTrace(); 
    }
        preparaGUI();
    }
      
   private void preparaGUI(){
    mainFrame = new JFrame("Fluxo de Potência Fundamental");
    mainFrame.setSize(1200,700);
    mainFrame.setLayout(null);
    mainFrame.setLocationRelativeTo(null);
   
    
    mainFrame.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent windowEvent){
	    System.exit(0);
         }        
      });
    
    entradasTensao = new JPanel();
    entradasTensao.setLayout(null);
    entradasTensao.setBounds(20,30,270,130);
    entradasTensao.setBorder(javax.swing.BorderFactory.createTitledBorder("Tensão"));
    
    entradasCorrente = new JPanel();
    entradasCorrente.setLayout(null);
    entradasCorrente.setBorder(javax.swing.BorderFactory.createTitledBorder("Corrente"));
    entradasCorrente.setBounds(300,30,270,130);
    
    panelGrafico = new JPanel();
    panelGrafico.setLayout(null);
    panelGrafico.setBorder(javax.swing.BorderFactory.createTitledBorder("Gráficos"));
    panelGrafico.setBounds(580,30,600,625);
    
    saidas = new JPanel();
    saidas.setLayout(null);
    saidas.setBorder(javax.swing.BorderFactory.createTitledBorder("Saídas"));
    saidas.setBounds(20,170,550,150);
      
    trianguloPotencias = new JPanel();
    trianguloPotencias.setLayout(null);
    trianguloPotencias.setBorder(javax.swing.BorderFactory.createTitledBorder("Triângulo Potências"));
    trianguloPotencias.setBounds(20,330,550,325);
    
    preparaJPanels();
    mainFrame.add(entradasTensao);
    mainFrame.add(entradasCorrente);
    mainFrame.add(panelGrafico);
    mainFrame.add(saidas);
    mainFrame.add(trianguloPotencias);
   
    mainFrame.setVisible(true);  
   }
   public void preparaJPanels(){
       Listeners = new AcoesUc2();
       //Entradas
        JLabel labelAmpTensao = new JLabel("Amplitude (Tensão):");
        JLabel labelAngTensao = new JLabel("Ângulo da Fase (Tensão):");
        JButton okTensaoButton = new JButton("OK");
        JLabel labelAmpCorrente = new JLabel("Amplitude (Corrente):");
        JLabel labelAngCorrente = new JLabel("Ângulo da Fase (Corrente):");
        JButton okCorrenteButton = new JButton("OK");
        
        labelAmpTensao.setBounds(15,30,200,20);
        labelAngTensao.setBounds(15,60,200,20);
        labelAmpCorrente.setBounds(15,30,200,20);
        labelAngCorrente.setBounds(15,60,200,20);
        okTensaoButton.setBounds(110,90,54,30);     
        okCorrenteButton.setBounds(110,90,54,30);
       
        MaskFormatter entraAmpTensao = null; 
        MaskFormatter entraAmpCorrente = null; 
             
        try{
            entraAmpTensao = new MaskFormatter("###");
            entraAmpCorrente = new MaskFormatter("###");
   
        }
        catch(ParseException excp) {
            System.err.println("Erro na formatação: " + excp.getMessage());
            System.exit(-1);
        }
     
        JFormattedTextField jFormattedAmpTensao = new JFormattedTextField(entraAmpTensao);
        JFormattedTextField jFormattedAmpCorrente = new JFormattedTextField(entraAmpCorrente);
        jFormattedAmpTensao.setValue("000");
        jFormattedAmpCorrente.setValue("000");
        jFormattedAmpTensao.setToolTipText("É necessário colocar 3 números. Se o número for 1, insira 001.");
        
        
        JComboBox jFormattedAngTensao = new JComboBox();
        JComboBox jFormattedAngCorrente = new JComboBox();
        for (int i = -180; i<181; i++){
            jFormattedAngTensao.addItem(i);
            jFormattedAngCorrente.addItem(i);
        }
        jFormattedAngTensao.setSelectedIndex(180);
        jFormattedAngCorrente.setSelectedIndex(180);

        okTensaoButton.setActionCommand("okTensao");
        okTensaoButton.setToolTipText("Clique para inserir os dados.");
        okCorrenteButton.setActionCommand("okCorrente");
        okCorrenteButton.setToolTipText("Clique para inserir os dados.");
     
        jFormattedAmpTensao.setBounds(225,30,35,22);
        jFormattedAngTensao.setBounds(200,60,58,20);
        jFormattedAmpCorrente.setBounds(225,30,35,22);
        jFormattedAngCorrente.setBounds(200,60,58,20);
        
     
        entradasTensao.add(labelAmpTensao);
        entradasTensao.add(jFormattedAmpTensao);
        okCorrenteButton.addActionListener(Listeners);
        okTensaoButton.addActionListener(Listeners);
        entradasTensao.add(labelAngTensao);
        entradasTensao.add(jFormattedAngTensao);
        entradasTensao.add(okTensaoButton);  
        entradasCorrente.add(labelAmpCorrente);
        entradasCorrente.add(labelAngCorrente);
        entradasCorrente.add(jFormattedAmpCorrente);
        entradasCorrente.add(jFormattedAngCorrente);
        entradasCorrente.add(okCorrenteButton);  
        //Fim das entradas
        
        //Saidas
        JLabel labelPotAtiva = new JLabel("Potência Ativa:");
        JLabel labelPotReativa = new JLabel("Potência Reativa:");
        JLabel labelPotAparente = new JLabel("Potência Aparente:");
        JLabel labelFatPotencia = new JLabel("Fator de Potência:");
        
        labelPotAtiva.setBounds(20,30,200,25);
        labelPotReativa.setBounds(20,55,200,25);
        labelPotAparente.setBounds(20,80,200,25);
        labelFatPotencia.setBounds(20,105,200,25);
                 
        JTextField jtextPotAtiva = new JTextField();
        JTextField jtextPotReativa = new JTextField();
        JTextField jtextPotAparente = new JTextField();
        JTextField jtextFatPotencia = new JTextField();
        
        jtextPotAtiva.setBounds(300,20,200,25);
        jtextPotReativa.setBounds(300,50,200,25);
        jtextPotAparente.setBounds(300,80,200,25);
        jtextFatPotencia.setBounds(300,110,200,25);
        
        jtextPotAtiva.setEditable(false);
        jtextPotReativa.setEditable(false);
        jtextPotAparente.setEditable(false);
        jtextFatPotencia.setEditable(false);
        
        jtextPotAtiva.setHorizontalAlignment(SwingConstants.RIGHT);
        jtextPotReativa.setHorizontalAlignment(SwingConstants.RIGHT);
        jtextPotAparente.setHorizontalAlignment(SwingConstants.RIGHT);
        jtextFatPotencia.setHorizontalAlignment(SwingConstants.RIGHT);

           
        saidas.add(labelPotAtiva);
        saidas.add(labelPotReativa);
        saidas.add(labelPotAparente);
        saidas.add(labelFatPotencia);
        saidas.add(jtextPotAtiva);
        saidas.add(jtextPotReativa);
        saidas.add(jtextPotAparente);
        saidas.add(jtextFatPotencia);
        //Fim das saidas
        
        //Gráficos
        Uc2 graficoOndaTensao = new Uc2();
        Uc2 graficoOndaCorrente = new Uc2();
        Uc2 graficoOndaPotInst = new Uc2();
        
        ArrayList<Double> ondaTensao1 = graficoOndaTensao.ondaTensao();
        ArrayList<Double> ondaCorrente = graficoOndaCorrente.ondaCorrente();
        ArrayList<Double> ondaPotInst = graficoOndaPotInst.ondaPotInst();
        
        Grafico graficoTensao = new Grafico(ondaTensao1,false);
        Grafico graficoCorrente = new Grafico(ondaCorrente,false);
        Grafico graficoPotInst = new Grafico(ondaPotInst,true);
        
        JLabel labelondaTensao = new JLabel("Forma de onda da Tensão");
        JLabel labelondaCorente = new JLabel("Forma de onda da Corrente");
        JLabel labelondaPotInst = new JLabel("Forma de onda da Potência Instantânea:");
        
        graficoTensao.setBounds(25,25,550,160);
        graficoCorrente.setBounds(25,225,550,160);
        graficoPotInst.setBounds(25,425,550,160);
        
        
        
        labelondaTensao.setBounds(200,190,300,20);
        labelondaTensao.setFont(new Font("Dialog", Font.PLAIN, 14));
        labelondaCorente.setBounds(200,390,300,20);
        labelondaCorente.setFont(new Font("Dialog", Font.PLAIN, 14));
        labelondaPotInst.setBounds(165,590,300,20);
        labelondaPotInst.setFont(new Font("Dialog", Font.PLAIN, 14));
               
        panelGrafico.add(graficoTensao);
        panelGrafico.add(graficoCorrente);
        panelGrafico.add(graficoPotInst);
        panelGrafico.add(labelondaTensao);
        panelGrafico.add(labelondaCorente);
        panelGrafico.add(labelondaPotInst);
        //Fim dos Graficos
        
        //Triangulo de potências
        JLabel labelInformacoes = new JLabel();
        JLabel labelinfoPotAparente = new JLabel();
        JLabel labelinfoPotReativa = new JLabel();
        JLabel labelinfoPotAtiva = new JLabel();
        
        labelInformacoes.setText("Onde:");
        labelinfoPotAparente.setText("<html><font color='blue'>Linha Azul:</font> Potência Aparente</html>");
        labelinfoPotReativa.setText("<html><font color='black'>Linha Preta:</font> Potência Reativa</html>");
        labelinfoPotAtiva.setText("<html><font color='red'>Linha Vermelha:</font> Potência Ativa</html>");
        
        labelInformacoes.setBounds(325,25,200,35);
        labelinfoPotAparente.setBounds(325,75,200,35);
        labelinfoPotReativa.setBounds(325,125,200,35);
        labelinfoPotAtiva.setBounds(325,175,200,35);
        
        TrianguloPotencias tripot = new TrianguloPotencias(-2,0);
        tripot.setBounds(25,25,280,280);
        
        trianguloPotencias.add(tripot);
        trianguloPotencias.add(labelInformacoes);
        trianguloPotencias.add(labelinfoPotAparente);
        trianguloPotencias.add(labelinfoPotReativa);
        trianguloPotencias.add(labelinfoPotAtiva);
        //Fim do Triangulo de Potencias
        
        //Voltar
        JLabel tituloUc2 = new JLabel("Simular Fluxo de Potência Fundamental");
        JButton voltar = new JButton("Voltar");
        voltar.setBounds(1125,1,54,30);
        voltar.setActionCommand("voltarUc2");
        tituloUc2.setBounds(50,1,1000,30);
        tituloUc2.setFont(new Font("Dialog", Font.BOLD, 14));
        voltar.addActionListener(Listeners);
        mainFrame.add(voltar);
        mainFrame.add(tituloUc2);
        //Fim do Voltar
        
        
        Listeners.entradasCorrente(jFormattedAmpCorrente,jFormattedAngCorrente,graficoCorrente); 
        Listeners.entradasTensao(jFormattedAmpTensao, jFormattedAngTensao, graficoTensao); 
        Listeners.saidas(mainFrame, jtextPotAtiva, jtextPotReativa, jtextPotAparente, jtextFatPotencia, graficoPotInst, tripot);
   
   }

}




