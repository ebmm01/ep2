package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Uc2;
import view.MenuPrincipal;
public class AcoesUc2  implements ActionListener {
        Uc2 calculos = new Uc2();
        private JFrame mainFrame;
	private JFormattedTextField jFormattedAmpTensao;
	private JFormattedTextField jFormattedAmpCorrente;
	private JComboBox jFormattedAngTensao;
      	private JComboBox jFormattedAngCorrente;
        private JTextField jtextPotAtiva;
        private JTextField jtextPotReativa;
        private JTextField jtextPotAparente;
        private JTextField jtextFatPotencia;
        private Grafico graficoTensao;
	private Grafico graficoCorrente;
        private Grafico graficoPotInst;
        private TrianguloPotencias tripot;
        
	
        
        public void entradasTensao(JFormattedTextField jFormattedAmpTensao,JComboBox jFormattedAngTensao,Grafico graficoTensao){
            this.jFormattedAmpTensao=jFormattedAmpTensao;
            this.jFormattedAngTensao=jFormattedAngTensao;
            this.graficoTensao=graficoTensao;
           
        }
        
        public void entradasCorrente(JFormattedTextField jFormattedAmpCorrente,JComboBox jFormattedAngCorrente,Grafico graficoCorrente){
            this.jFormattedAmpCorrente=jFormattedAmpCorrente;
            this.jFormattedAngCorrente=jFormattedAngCorrente;
            this.graficoCorrente=graficoCorrente;
        }
        
        public void saidas(JFrame mainFrame,JTextField jtextPotAtiva,JTextField jtextPotReativa,JTextField jtextPotAparente,JTextField jtextFatPotencia,Grafico graficoPotInst, TrianguloPotencias tripot) {
                this.mainFrame=mainFrame;
		this.jtextPotAtiva=jtextPotAtiva;
                this.jtextPotReativa=jtextPotReativa;
                this.jtextPotAparente=jtextPotAparente;
                this.jtextFatPotencia=jtextFatPotencia;
                this.graficoPotInst=graficoPotInst;
                this.tripot=tripot;
	}
        public void actionPerformed(ActionEvent e) {
	    double ampTensao, ampCorrente, angTensao, angCorrente;
             
            ampTensao = Double.parseDouble(jFormattedAmpTensao.getText());
            angTensao = Double.parseDouble(jFormattedAngTensao.getSelectedItem().toString());
            ampCorrente = Double.parseDouble(jFormattedAmpCorrente.getText());
            angCorrente = Double.parseDouble(jFormattedAngCorrente.getSelectedItem().toString());
                     
            calculos.setAmpTensao(ampTensao);
            calculos.setAmpCorrente(ampCorrente);
            calculos.setAngTensao(angTensao);
            calculos.setAngCorrente(angCorrente);
                                   
           
           
	String command = e.getActionCommand();  
         if( command.equals("okCorrente"))  {
             if (ampCorrente < 101){         
            jtextPotAtiva.setText(String.valueOf(calculos.PotAtiva())+" Watt");
            jtextPotReativa.setText(String.valueOf(calculos.PotReativa())+" var");
            jtextPotAparente.setText(String.valueOf(calculos.PotAparente())+" VA");
            jtextFatPotencia.setText(String.valueOf(calculos.FatPotencia())+" "+calculos.stringFatPotencia());
            graficoTensao.setScores(calculos.ondaTensao());
            graficoCorrente.setScores(calculos.ondaCorrente());
            graficoPotInst.setScores(calculos.ondaPotInst());
            tripot.setCoordenadas(calculos.PotAtiva()/100 , calculos.PotReativa()/100);
             }
             else JOptionPane.showMessageDialog(null,"Valor da Amplitude inválido!! (>100)");
         }
         if( command.equals("okTensao"))  {
           if(ampTensao < 221 ){
            jtextPotAtiva.setText(String.valueOf(calculos.PotAtiva())+" Watt");
            jtextPotReativa.setText(String.valueOf(calculos.PotReativa())+" var");
            jtextPotAparente.setText(String.valueOf(calculos.PotAparente())+" VA");
            jtextFatPotencia.setText(String.valueOf(calculos.FatPotencia())+" "+calculos.stringFatPotencia());
            graficoTensao.setScores(calculos.ondaTensao());
            graficoCorrente.setScores(calculos.ondaCorrente());
            graficoPotInst.setScores(calculos.ondaPotInst());
            tripot.setCoordenadas(calculos.PotAtiva()/100 ,calculos.PotReativa()/100);
         }
          else JOptionPane.showMessageDialog(null,"Valor da Amplitude inválido!! (>220)");
         }
          if( command.equals("voltarUc2")){
              mainFrame.dispose();
                try {
                    MenuPrincipal voltar = new MenuPrincipal();
                } catch (IOException ex) {
                    Logger.getLogger(AcoesUc2.class.getName()).log(Level.SEVERE, null, ex);
                }
          }
	}

	
	
}