package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Uc4;
import view.MenuPrincipal;
public class AcoesUc4  implements ActionListener {
        Uc4 calculosUc4 = new Uc4();
        private JFrame mainFrame;
	private JPanel panelFund;
        private JPanel panelHarmonico;
        private JPanel panelSaidas;
	private JFormattedTextField jFormattedAmpFund;
        private JFormattedTextField jFormattedAmpHarmonico;
	private JComboBox jcomboAngFund;
      	private JComboBox jcomboAngHarmonico;
        private JComboBox jcomboOrdemHarmonico;
        private JTextField textPotLiquid;
        private JTextField textPotDistor;
        private JTextField textTPF;
        private Grafico graficoFund;
	private Grafico Harmonico;
        private Grafico graficoresulaPotInst;
        
	public void Saidas(JFrame mainFrame,JTextField textPotLiquid,JTextField textPotDistor,JTextField textTPF, Grafico graficoresulaPotInst ) {
            this.mainFrame=mainFrame;
            this.textPotLiquid=textPotLiquid;
            this.textPotDistor=textPotDistor;
            this.textTPF=textTPF;
            this.graficoresulaPotInst=graficoresulaPotInst;
    }
	public void entradaFundamental(JFormattedTextField jFormattedAmpFund,JComboBox jcomboAngFund, Grafico graficoFund){
            this.jFormattedAmpFund=jFormattedAmpFund;
            this.jcomboAngFund=jcomboAngFund;
            this.graficoFund=graficoFund;
        }
        
        public void entradaHarmonico(JFormattedTextField jFormattedAmpHarmonico,JComboBox jcomboAngHarmonico,JComboBox jcomboOrdemHarmonico,Grafico Harmonico){
            this.jFormattedAmpHarmonico=jFormattedAmpHarmonico;
            this.jcomboAngHarmonico=jcomboAngHarmonico;
            this.jcomboOrdemHarmonico=jcomboOrdemHarmonico;
            this.Harmonico=Harmonico;
        }
	
	public void actionPerformed(ActionEvent e) {
	    double ampFund, ampHarmonico, angFund, angHarmonico, ordemHarmonico;
             
            ampFund = Double.parseDouble(jFormattedAmpFund.getText());
            angFund = Double.parseDouble(jcomboAngFund.getSelectedItem().toString());
            ampHarmonico = Double.parseDouble(jFormattedAmpHarmonico.getText());
            angHarmonico = Double.parseDouble(jcomboAngHarmonico.getSelectedItem().toString());
            ordemHarmonico = Double.parseDouble(jcomboOrdemHarmonico.getSelectedItem().toString());
                     
            calculosUc4.setAmpFund(ampFund);
            calculosUc4.setAngFund(angFund);
            calculosUc4.setAmpHarmonico(ampHarmonico);
            calculosUc4.setAngHarmonico(angHarmonico);
            calculosUc4.setOrdemHarmonico(ordemHarmonico);
                        
   

	String command = e.getActionCommand();  
         if( command.equals("okFund"))  {
             if (ampFund < 221){         
            textPotLiquid.setText(String.valueOf(calculosUc4.PotLiquida())+" Watt");
            textPotDistor.setText(String.valueOf(calculosUc4.PotDistorcao())+" Volt-Ampere");
            textTPF.setText(String.valueOf(calculosUc4.TPF()));
           
            graficoFund.setScores(calculosUc4.ondaTensao());
            Harmonico.setScores(calculosUc4.ondaHarmonico());
            graficoresulaPotInst.setScores(calculosUc4.ondaPotInstantanea());
             }
             else JOptionPane.showMessageDialog(null,"Valor da Amplitude inválido!! (>220)");
         }
         if( command.equals("okHarmonico"))  {
           if(ampHarmonico < 101 ){
            textPotLiquid.setText(String.valueOf(calculosUc4.PotLiquida())+" Watt");
            textPotDistor.setText(String.valueOf(calculosUc4.PotDistorcao())+" Volt-Ampere");
            textTPF.setText(String.valueOf(calculosUc4.TPF()));
                     
            graficoFund.setScores(calculosUc4.ondaTensao());
            Harmonico.setScores(calculosUc4.ondaHarmonico());
            graficoresulaPotInst.setScores(calculosUc4.ondaPotInstantanea());
         }
          else JOptionPane.showMessageDialog(null,"Valor da Amplitude inválido!! (>100)");
         }
          if( command.equals("voltarUc4")){
              mainFrame.dispose();
                try {
                    MenuPrincipal voltar = new MenuPrincipal();
                } catch (IOException ex) {
                    Logger.getLogger(AcoesUc4.class.getName()).log(Level.SEVERE, null, ex);
                }
          }
	}

	
	
}