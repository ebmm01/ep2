package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import view.InterfaceUc2;
import view.InterfaceUc3;
import view.InterfaceUc4;


public class AcoesMenuPrincipal  implements ActionListener {
    private JFrame mainFrame;
               
	public AcoesMenuPrincipal(JFrame mainFrame) {
            this.mainFrame=mainFrame;
            
	}
	
	
	public void actionPerformed(ActionEvent e) {
	   
	String command = e.getActionCommand();  
        if( command.equals("okUc2"))  {
          mainFrame.dispose();
            try {
                InterfaceUc2 iniciar = new InterfaceUc2();
            } catch (IOException ex) {
                Logger.getLogger(AcoesMenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
        
        if( command.equals("okUc3"))  {
         mainFrame.dispose();
            try {
                InterfaceUc3 iniciar = new InterfaceUc3();
            } catch (IOException ex) {
                Logger.getLogger(AcoesMenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
	
        
        if( command.equals("okUc4"))  {
         mainFrame.dispose();
            try {
                InterfaceUc4 iniciar = new InterfaceUc4();
            } catch (IOException ex) {
                Logger.getLogger(AcoesMenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
         }
	
    }

	
}