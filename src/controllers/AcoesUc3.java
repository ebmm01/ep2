package controllers;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import model.Uc3;
import view.MenuPrincipal;

public class AcoesUc3  implements ActionListener {
        Uc3 calculosUc3 = new Uc3();
        private JFrame mainFrame;
	private JFormattedTextField jFormattedAmpCompFund;
        private JFormattedTextField jFormattedAmpHarmonico1;
        private JFormattedTextField jFormattedAmpHarmonico2;
        private JFormattedTextField jFormattedAmpHarmonico3;
        private JFormattedTextField jFormattedAmpHarmonico4;
        private JFormattedTextField jFormattedAmpHarmonico5;
	private JFormattedTextField jFormattedAmpHarmonico6;
	private JComboBox jcomboCompFund;
        private JComboBox jcomboNHarmonicos;
        private JComboBox jcomboAngHarmonico1;
        private JComboBox jcomboAngHarmonico2;
        private JComboBox jcomboAngHarmonico3;
        private JComboBox jcomboAngHarmonico4;
        private JComboBox jcomboAngHarmonico5;
        private JComboBox jcomboAngHarmonico6;
        private JComboBox jcomboOrdemHarmonico1;
        private JComboBox jcomboOrdemHarmonico2;
        private JComboBox jcomboOrdemHarmonico3;
        private JComboBox jcomboOrdemHarmonico4;
      	private JComboBox jcomboOrdemHarmonico5;
        private JComboBox jcomboOrdemHarmonico6;
        private Grafico graficoCompFund;
	private Grafico graficoResultante;
        private Grafico graficoHarmonico1;
        private Grafico graficoHarmonico2;
        private Grafico graficoHarmonico3;
        private Grafico graficoHarmonico4;
        private Grafico graficoHarmonico5;
        private Grafico graficoHarmonico6;
        private JTextArea resultante;
        
	public void saidas(JFrame mainFrame, JTextArea resultante) {
                this.mainFrame=mainFrame;
		this.resultante=resultante;	                
	}
        
        public void entradasCompFund(JFormattedTextField jFormattedAmpCompFund,JComboBox jcomboCompFund,Grafico graficoCompFund){
            this.jFormattedAmpCompFund=jFormattedAmpCompFund;
            this.jcomboCompFund=jcomboCompFund;
            this.graficoCompFund=graficoCompFund;
        }
        
        public void entradasHarmonicos(JFormattedTextField jFormattedAmpHarmonico1,
                JFormattedTextField jFormattedAmpHarmonico2,
                JFormattedTextField jFormattedAmpHarmonico3,
                JFormattedTextField jFormattedAmpHarmonico4,
                JFormattedTextField jFormattedAmpHarmonico5,
                JFormattedTextField jFormattedAmpHarmonico6,
                JComboBox jcomboNHarmonicos,
                JComboBox jcomboAngHarmonico1,
                JComboBox jcomboAngHarmonico2,
                JComboBox jcomboAngHarmonico3,
                JComboBox jcomboAngHarmonico4,
                JComboBox jcomboAngHarmonico5,
                JComboBox jcomboAngHarmonico6,
                JComboBox jcomboOrdemHarmonico1,
                JComboBox jcomboOrdemHarmonico2,
                JComboBox jcomboOrdemHarmonico3,
                JComboBox jcomboOrdemHarmonico4,
                JComboBox jcomboOrdemHarmonico5,
                JComboBox jcomboOrdemHarmonico6){
                this.jFormattedAmpHarmonico1=jFormattedAmpHarmonico1;
                this.jFormattedAmpHarmonico2=jFormattedAmpHarmonico2;
                this.jFormattedAmpHarmonico3=jFormattedAmpHarmonico3;
                this.jFormattedAmpHarmonico4=jFormattedAmpHarmonico4;
                this.jFormattedAmpHarmonico5=jFormattedAmpHarmonico5;
                this.jFormattedAmpHarmonico6=jFormattedAmpHarmonico6;
                this.jcomboNHarmonicos=jcomboNHarmonicos;
                this.jcomboAngHarmonico1=jcomboAngHarmonico1;
                this.jcomboAngHarmonico2=jcomboAngHarmonico2;
                this.jcomboAngHarmonico3=jcomboAngHarmonico3;
                this.jcomboAngHarmonico4=jcomboAngHarmonico4;
                this.jcomboAngHarmonico5=jcomboAngHarmonico5;
                this.jcomboAngHarmonico6=jcomboAngHarmonico6;
                this.jcomboOrdemHarmonico1=jcomboOrdemHarmonico1;
                this.jcomboOrdemHarmonico2=jcomboOrdemHarmonico2;
                this.jcomboOrdemHarmonico3=jcomboOrdemHarmonico3;
                this.jcomboOrdemHarmonico4=jcomboOrdemHarmonico4;
                this.jcomboOrdemHarmonico5=jcomboOrdemHarmonico5;
                this.jcomboOrdemHarmonico6=jcomboOrdemHarmonico6;
            
        }
        
        public void graficosHarmonicos(
                Grafico graficoHarmonico1,
                Grafico graficoHarmonico2,
                Grafico graficoHarmonico3,
                Grafico graficoHarmonico4,
                Grafico graficoHarmonico5,
                Grafico graficoHarmonico6){
            
            this.graficoHarmonico1=graficoHarmonico1;
            this.graficoHarmonico2=graficoHarmonico2;
            this.graficoHarmonico3=graficoHarmonico3;
            this.graficoHarmonico4=graficoHarmonico4;
            this.graficoHarmonico5=graficoHarmonico5;
        }
        
        public void graficoResultante(Grafico graficoResultante){
            this.graficoResultante=graficoResultante;
        }
	
	public void actionPerformed(ActionEvent e) {
          double ampCompFund,angCompFund,
                  ampHarmonico1, angHarmonico1, ordemHarmonico1,
                  ampHarmonico2, angHarmonico2, ordemHarmonico2,
                  ampHarmonico3, angHarmonico3, ordemHarmonico3,
                  ampHarmonico4, angHarmonico4, ordemHarmonico4,
                  ampHarmonico5, angHarmonico5, ordemHarmonico5,
                  ampHarmonico6, angHarmonico6, ordemHarmonico6;
          String serieresultante;
          
        ampCompFund = Double.parseDouble(jFormattedAmpCompFund.getText());
        angCompFund = Double.parseDouble(jcomboCompFund.getSelectedItem().toString());
          
        ampHarmonico1 = Double.parseDouble(jFormattedAmpHarmonico1.getText());
        angHarmonico1 = Double.parseDouble(jcomboAngHarmonico1.getSelectedItem().toString());
        ordemHarmonico1 = Double.parseDouble(jcomboOrdemHarmonico1.getSelectedItem().toString());
          
        ampHarmonico2 = Double.parseDouble(jFormattedAmpHarmonico2.getText());
        angHarmonico2 = Double.parseDouble(jcomboAngHarmonico2.getSelectedItem().toString());
        ordemHarmonico2 = Double.parseDouble(jcomboOrdemHarmonico2.getSelectedItem().toString());
          
        ampHarmonico3 = Double.parseDouble(jFormattedAmpHarmonico3.getText());
        angHarmonico3 = Double.parseDouble(jcomboAngHarmonico3.getSelectedItem().toString());
        ordemHarmonico3 = Double.parseDouble(jcomboOrdemHarmonico3.getSelectedItem().toString());
          
        ampHarmonico4 = Double.parseDouble(jFormattedAmpHarmonico4.getText());
        angHarmonico4 = Double.parseDouble(jcomboAngHarmonico4.getSelectedItem().toString());
        ordemHarmonico4 = Double.parseDouble(jcomboOrdemHarmonico4.getSelectedItem().toString());
          
        ampHarmonico5 = Double.parseDouble(jFormattedAmpHarmonico5.getText());
        angHarmonico5 = Double.parseDouble(jcomboAngHarmonico5.getSelectedItem().toString());
        ordemHarmonico5 = Double.parseDouble(jcomboOrdemHarmonico5.getSelectedItem().toString());
          
        ampHarmonico6 = Double.parseDouble(jFormattedAmpHarmonico6.getText());
        angHarmonico6 = Double.parseDouble(jcomboAngHarmonico6.getSelectedItem().toString());
        ordemHarmonico6 = Double.parseDouble(jcomboOrdemHarmonico6.getSelectedItem().toString());
        
        calculosUc3.setAmpCompFund(ampCompFund);
        calculosUc3.setAngCompFund(angCompFund);
        
        calculosUc3.setAmpHarmonico1(ampHarmonico1);
        calculosUc3.setAngHarmonico1(angHarmonico1);
        calculosUc3.setOrdemHarmonica1(ordemHarmonico1);
        
        calculosUc3.setAmpHarmonico2(ampHarmonico2);
        calculosUc3.setAngHarmonico2(angHarmonico2);
        calculosUc3.setOrdemHarmonica2(ordemHarmonico2);
        
        calculosUc3.setAmpHarmonico3(ampHarmonico3);
        calculosUc3.setAngHarmonico3(angHarmonico3);
        calculosUc3.setOrdemHarmonica3(ordemHarmonico3);
        
        calculosUc3.setAmpHarmonico4(ampHarmonico4);
        calculosUc3.setAngHarmonico4(angHarmonico4);
        calculosUc3.setOrdemHarmonica4(ordemHarmonico4);
        
        calculosUc3.setAmpHarmonico5(ampHarmonico5);
        calculosUc3.setAngHarmonico5(angHarmonico5);
        calculosUc3.setOrdemHarmonica5(ordemHarmonico5);
        
        calculosUc3.setAmpHarmonico6(ampHarmonico6);
        calculosUc3.setAngHarmonico6(angHarmonico6);
        calculosUc3.setOrdemHarmonica6(ordemHarmonico6);
        
        serieresultante = calculosUc3.serie();
                     
        resultante.setText(serieresultante);
        
        String command = e.getActionCommand();  
        
        if( command.equals("okHarmonicos"))  {
            if (ampHarmonico1<221 && ampHarmonico2<221 && ampHarmonico3<221 && ampHarmonico4<221 && ampHarmonico5<221 && ampHarmonico6<221){
                graficoResultante.setScores(calculosUc3.ondaResultante());
                graficoHarmonico1.setScores(calculosUc3.ondaHarmonico1());
                graficoHarmonico2.setScores(calculosUc3.ondaHarmonico2());
                graficoHarmonico3.setScores(calculosUc3.ondaHarmonico3());
                graficoHarmonico4.setScores(calculosUc3.ondaHarmonico4());
                graficoHarmonico5.setScores(calculosUc3.ondaHarmonico5());
                graficoHarmonico6.setScores(calculosUc3.ondaHarmonico6());
                graficoResultante.repaint();
                graficoResultante.revalidate();
                resultante.setText(serieresultante);
                resultante.revalidate();

                resultante.revalidate();
            }
            else JOptionPane.showMessageDialog(null,"Valor da Amplitude inválido!! (>220)");
	}
        
        if( command.equals("okCompFund"))  {
            if (ampCompFund<221){
                graficoCompFund.setScores(calculosUc3.ondaTensao());
                graficoResultante.setScores(calculosUc3.ondaResultante());
                resultante.setText(serieresultante);
                resultante.revalidate();
            }
            else JOptionPane.showMessageDialog(null,"Valor da Amplitude inválido!! (>220)");
	}
        if( command.equals("voltarUc3")){
              mainFrame.dispose();
                try {
                    MenuPrincipal voltar = new MenuPrincipal();
                } catch (IOException ex) {
                    Logger.getLogger(AcoesUc3.class.getName()).log(Level.SEVERE, null, ex);
                }
          }
        
        JComboBox jcomboNHarmonicos = (JComboBox) e.getSource();

        Object selected = jcomboNHarmonicos.getSelectedItem();
        if(selected.toString().equals("0")){
            jcomboAngHarmonico1.setEnabled(false);
            jFormattedAmpHarmonico1.setEnabled(false);
            jcomboOrdemHarmonico1.setEnabled(false);
            jcomboAngHarmonico2.setEnabled(false);
            jFormattedAmpHarmonico2.setEnabled(false);
            jcomboOrdemHarmonico2.setEnabled(false);
            jcomboAngHarmonico3.setEnabled(false);
            jFormattedAmpHarmonico3.setEnabled(false);
            jcomboOrdemHarmonico3.setEnabled(false);
            jcomboAngHarmonico4.setEnabled(false);
            jFormattedAmpHarmonico4.setEnabled(false);
            jcomboOrdemHarmonico4.setEnabled(false);
            jcomboAngHarmonico5.setEnabled(false);
            jFormattedAmpHarmonico5.setEnabled(false);
            jcomboOrdemHarmonico5.setEnabled(false);
            jcomboAngHarmonico6.setEnabled(false);
            jFormattedAmpHarmonico6.setEnabled(false);
            jcomboOrdemHarmonico6.setEnabled(false);
            
            jcomboAngHarmonico1.setSelectedItem(0);
            jcomboAngHarmonico2.setSelectedItem(0);
            jcomboAngHarmonico3.setSelectedItem(0);
            jcomboAngHarmonico4.setSelectedItem(0);
            jcomboAngHarmonico5.setSelectedItem(0);
            jcomboAngHarmonico6.setSelectedItem(0);
            
            jcomboOrdemHarmonico1.setSelectedItem(0);
            jcomboOrdemHarmonico2.setSelectedItem(0);
            jcomboOrdemHarmonico3.setSelectedItem(0);
            jcomboOrdemHarmonico4.setSelectedItem(0);
            jcomboOrdemHarmonico5.setSelectedItem(0);
            jcomboOrdemHarmonico6.setSelectedItem(0);
                    
            jFormattedAmpHarmonico1.setValue("000");
            jFormattedAmpHarmonico2.setValue("000");
            jFormattedAmpHarmonico3.setValue("000");
            jFormattedAmpHarmonico4.setValue("000");
            jFormattedAmpHarmonico5.setValue("000");
            jFormattedAmpHarmonico6.setValue("000");
        }
        if(selected.toString().equals("1")){
            jcomboAngHarmonico1.setEnabled(true);
            jFormattedAmpHarmonico1.setEnabled(true);
            jcomboOrdemHarmonico1.setEnabled(true);
            jcomboAngHarmonico2.setEnabled(false);
            jFormattedAmpHarmonico2.setEnabled(false);
            jcomboOrdemHarmonico2.setEnabled(false);
            jcomboAngHarmonico3.setEnabled(false);
            jFormattedAmpHarmonico3.setEnabled(false);
            jcomboOrdemHarmonico3.setEnabled(false);
            jcomboAngHarmonico4.setEnabled(false);
            jFormattedAmpHarmonico4.setEnabled(false);
            jcomboOrdemHarmonico4.setEnabled(false);
            jcomboAngHarmonico5.setEnabled(false);
            jFormattedAmpHarmonico5.setEnabled(false);
            jcomboOrdemHarmonico5.setEnabled(false);
            jcomboAngHarmonico6.setEnabled(false);
            jFormattedAmpHarmonico6.setEnabled(false);
            jcomboOrdemHarmonico6.setEnabled(false);
            
            jcomboAngHarmonico2.setSelectedItem(0);
            jcomboAngHarmonico3.setSelectedItem(0);
            jcomboAngHarmonico4.setSelectedItem(0);
            jcomboAngHarmonico5.setSelectedItem(0);
            jcomboAngHarmonico6.setSelectedItem(0);
            
            jcomboOrdemHarmonico2.setSelectedItem(0);
            jcomboOrdemHarmonico3.setSelectedItem(0);
            jcomboOrdemHarmonico4.setSelectedItem(0);
            jcomboOrdemHarmonico5.setSelectedItem(0);
            jcomboOrdemHarmonico6.setSelectedItem(0);
            
            jFormattedAmpHarmonico2.setValue("000");
            jFormattedAmpHarmonico3.setValue("000");
            jFormattedAmpHarmonico4.setValue("000");
            jFormattedAmpHarmonico5.setValue("000");
            jFormattedAmpHarmonico6.setValue("000");
        }
        if(selected.toString().equals("2")){
            jcomboAngHarmonico1.setEnabled(true);
            jFormattedAmpHarmonico1.setEnabled(true);
            jcomboOrdemHarmonico1.setEnabled(true);
            jcomboAngHarmonico2.setEnabled(true);
            jFormattedAmpHarmonico2.setEnabled(true);
            jcomboOrdemHarmonico2.setEnabled(true);
            jcomboAngHarmonico3.setEnabled(false);
            jFormattedAmpHarmonico3.setEnabled(false);
            jcomboOrdemHarmonico3.setEnabled(false);
            jcomboAngHarmonico4.setEnabled(false);
            jFormattedAmpHarmonico4.setEnabled(false);
            jcomboOrdemHarmonico4.setEnabled(false);
            jcomboAngHarmonico5.setEnabled(false);
            jFormattedAmpHarmonico5.setEnabled(false);
            jcomboOrdemHarmonico5.setEnabled(false);
            jcomboAngHarmonico6.setEnabled(false);
            jFormattedAmpHarmonico6.setEnabled(false);
            jcomboOrdemHarmonico6.setEnabled(false);
                      
            jcomboAngHarmonico3.setSelectedItem(0);
            jcomboAngHarmonico4.setSelectedItem(0);
            jcomboAngHarmonico5.setSelectedItem(0);
            jcomboAngHarmonico6.setSelectedItem(0);
            
            jcomboOrdemHarmonico3.setSelectedItem(0);
            jcomboOrdemHarmonico4.setSelectedItem(0);
            jcomboOrdemHarmonico5.setSelectedItem(0);
            jcomboOrdemHarmonico6.setSelectedItem(0);
            
            jFormattedAmpHarmonico3.setValue("000");
            jFormattedAmpHarmonico4.setValue("000");
            jFormattedAmpHarmonico5.setValue("000");
            jFormattedAmpHarmonico6.setValue("000");
        }
        
        if(selected.toString().equals("3")){
            jcomboAngHarmonico1.setEnabled(true);
            jFormattedAmpHarmonico1.setEnabled(true);
            jcomboOrdemHarmonico1.setEnabled(true);
            jcomboAngHarmonico2.setEnabled(true);
            jFormattedAmpHarmonico2.setEnabled(true);
            jcomboOrdemHarmonico2.setEnabled(true);
            jcomboAngHarmonico3.setEnabled(true);
            jFormattedAmpHarmonico3.setEnabled(true);
            jcomboOrdemHarmonico3.setEnabled(true);
            jcomboAngHarmonico4.setEnabled(false);
            jFormattedAmpHarmonico4.setEnabled(false);
            jcomboOrdemHarmonico4.setEnabled(false);
            jcomboAngHarmonico5.setEnabled(false);
            jFormattedAmpHarmonico5.setEnabled(false);
            jcomboOrdemHarmonico5.setEnabled(false);
            jcomboAngHarmonico6.setEnabled(false);
            jFormattedAmpHarmonico6.setEnabled(false);
            jcomboOrdemHarmonico6.setEnabled(false);
            
            jcomboAngHarmonico4.setSelectedItem(0);
            jcomboAngHarmonico5.setSelectedItem(0);
            jcomboAngHarmonico6.setSelectedItem(0);
          
            jcomboOrdemHarmonico4.setSelectedItem(0);
            jcomboOrdemHarmonico5.setSelectedItem(0);
            jcomboOrdemHarmonico6.setSelectedItem(0);
            
            jFormattedAmpHarmonico4.setValue("000");
            jFormattedAmpHarmonico5.setValue("000");
            jFormattedAmpHarmonico6.setValue("000");
        }
        
        if(selected.toString().equals("4")){
            jcomboAngHarmonico1.setEnabled(true);
            jFormattedAmpHarmonico1.setEnabled(true);
            jcomboOrdemHarmonico1.setEnabled(true);
            jcomboAngHarmonico2.setEnabled(true);
            jFormattedAmpHarmonico2.setEnabled(true);
            jcomboOrdemHarmonico2.setEnabled(true);
            jcomboAngHarmonico3.setEnabled(true);
            jFormattedAmpHarmonico3.setEnabled(true);
            jcomboOrdemHarmonico3.setEnabled(true);
            jcomboAngHarmonico4.setEnabled(true);
            jFormattedAmpHarmonico4.setEnabled(true);
            jcomboOrdemHarmonico4.setEnabled(true);
            jcomboAngHarmonico5.setEnabled(false);
            jFormattedAmpHarmonico5.setEnabled(false);
            jcomboOrdemHarmonico5.setEnabled(false);
            jcomboAngHarmonico6.setEnabled(false);
            jFormattedAmpHarmonico6.setEnabled(false);
            jcomboOrdemHarmonico6.setEnabled(false);
                        
            jcomboAngHarmonico5.setSelectedItem(0);
            jcomboAngHarmonico6.setSelectedItem(0);
          
            jcomboOrdemHarmonico5.setSelectedItem(0);
            jcomboOrdemHarmonico6.setSelectedItem(0);
            
            jFormattedAmpHarmonico5.setValue("000");
            jFormattedAmpHarmonico6.setValue("000");
        }
        
        if(selected.toString().equals("5")){
            jcomboAngHarmonico1.setEnabled(true);
            jFormattedAmpHarmonico1.setEnabled(true);
            jcomboOrdemHarmonico1.setEnabled(true);
            jcomboAngHarmonico2.setEnabled(true);
            jFormattedAmpHarmonico2.setEnabled(true);
            jcomboOrdemHarmonico2.setEnabled(true);
            jcomboAngHarmonico3.setEnabled(true);
            jFormattedAmpHarmonico3.setEnabled(true);
            jcomboOrdemHarmonico3.setEnabled(true);
            jcomboAngHarmonico4.setEnabled(true);
            jFormattedAmpHarmonico4.setEnabled(true);
            jcomboOrdemHarmonico4.setEnabled(true);
            jcomboAngHarmonico5.setEnabled(true);
            jFormattedAmpHarmonico5.setEnabled(true);
            jcomboOrdemHarmonico5.setEnabled(true);
            jcomboAngHarmonico6.setEnabled(false);
            jFormattedAmpHarmonico6.setEnabled(false);
            jcomboOrdemHarmonico6.setEnabled(false);
            
            jcomboAngHarmonico6.setSelectedItem(0);
            
            jcomboOrdemHarmonico6.setSelectedItem(0);
            
            jFormattedAmpHarmonico6.setValue("000");
        }
        
        if(selected.toString().equals("6")){
            jcomboAngHarmonico1.setEnabled(true);
            jFormattedAmpHarmonico1.setEnabled(true);
            jcomboOrdemHarmonico1.setEnabled(true);
            jcomboAngHarmonico2.setEnabled(true);
            jFormattedAmpHarmonico2.setEnabled(true);
            jcomboOrdemHarmonico2.setEnabled(true);
            jcomboAngHarmonico3.setEnabled(true);
            jFormattedAmpHarmonico3.setEnabled(true);
            jcomboOrdemHarmonico3.setEnabled(true);
            jcomboAngHarmonico4.setEnabled(true);
            jFormattedAmpHarmonico4.setEnabled(true);
            jcomboOrdemHarmonico4.setEnabled(true);
            jcomboAngHarmonico5.setEnabled(true);
            jFormattedAmpHarmonico5.setEnabled(true);
            jcomboOrdemHarmonico5.setEnabled(true);
            jcomboAngHarmonico6.setEnabled(true);
            jFormattedAmpHarmonico6.setEnabled(true);
            jcomboOrdemHarmonico6.setEnabled(true);
        }
                
    }
        
}